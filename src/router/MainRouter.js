import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MoviesScreen from 'src/screen/MoviesScreen';
import CinemaScreen from 'src/screen/CinemaScreen';
import MapScreen from 'src/screen/MapScreen';
import Header from '../component/Header';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faMapMarkerAlt,
  faPlayCircle,
  faBars,
  faLocationArrow,
} from '@fortawesome/free-solid-svg-icons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MoreScreen from '../screen/MoreScreen';

const Tab = createMaterialBottomTabNavigator();
const MainRouter = () => {
  return (
    <>
      <Header />
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#3d81d6"
        shifting={false}
        inactiveColor="#ccc"
        unmountOnBlur={true}
        barStyle={{
          backgroundColor: '#fafafa',
          fontWeight: '700',
        }}>
        <Tab.Screen
          name="Movies"
          component={MoviesScreen}
          options={{
            tabBarLabel: 'Now Showing',
            labelStyle: {fontSize: 10},
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faPlayCircle} color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name="Cinema"
          component={CinemaScreen}
          options={{
            tabBarLabel: 'Cinema',
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faMapMarkerAlt} color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name="Map"
          component={MapScreen}
          options={{
            tabBarLabel: 'Map',
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faLocationArrow} color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name="More"
          component={MoreScreen}
          options={{
            tabBarLabel: 'More',
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faBars} color={color} size={20} />
            ),
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default MainRouter;
