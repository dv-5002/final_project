import React from 'react';
import {View, Text, Image, StatusBar} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import MainRouter from './MainRouter';
import MovieDetailScreen from 'src/screen/MovieDetailScreen';
import BookingScreen from 'src/screen/BookingScreen';
import CinemaDetailScreen from 'src/screen/CinemaDetailScreen';
import PurchaseScreen from 'src/screen/PurchaseScreen';
import HistoryScreen from 'src/screen/HistoryScreen';
import HistoryDetailScreen from 'src/screen/HistoryDetailScreen';
const Stack = createStackNavigator();
const StackRouter = () => {
  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="MainRouter" component={MainRouter} />
        <Stack.Screen name="MovieDetail" component={MovieDetailScreen} />
        <Stack.Screen name="Booking" component={BookingScreen} />
        <Stack.Screen name="CinemaDetail" component={CinemaDetailScreen} />
        <Stack.Screen name="Purchase" component={PurchaseScreen} />
        <Stack.Screen name="History" component={HistoryScreen} />
        <Stack.Screen name="HistoryDetail" component={HistoryDetailScreen} />
      </Stack.Navigator>
    </>
  );
};

export default StackRouter;
