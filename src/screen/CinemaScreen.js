import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';

const CinemaScreen = () => {
  const [cinema, setCinema] = useState([]);
  const [loading, setLoading] = useState();
  const navigation = useNavigation();
  useEffect(() => {
    fetchCinema();
  }, []);

  const fetchCinema = () => {
    setLoading(true);
    axios
      .get('http://movietome.unomy.pw/cinema')
      .then(response => {
        console.log(response);
        setCinema(response.data);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  const CinemaCard = ({name, id}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('CinemaDetail', {
            cinemaId: id,
          });
        }}
        activeOpacity={0.6}
        style={{
          padding: 10,
          backgroundColor: '#e8edf1',
          borderBottomColor: '#ccc',
          borderBottomWidth: 1,
        }}>
        <Text>{name}</Text>
      </TouchableOpacity>
    );
  };
  console.log(cinema);
  return (
    <SafeAreaView style={{flex: 1}}>
      {loading ? (
        <ActivityIndicator
          animating={loading}
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
          size="large"
        />
      ) : (
        <FlatList
          style={{marginTop: 10}}
          data={cinema ? cinema : []}
          renderItem={({item}) => <CinemaCard name={item.name} id={item._id} />}
          keyExtractor={item => item._id}
        />
      )}
    </SafeAreaView>
  );
};

export default CinemaScreen;
