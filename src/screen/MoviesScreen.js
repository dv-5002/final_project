import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  RefreshControl,
  ScrollView,
  StyleSheet,
  View,
  FlatList,
} from 'react-native';
import Header from 'src/component/Header';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import MoviePoster from 'src/component/MoviePoster';
import MovieDetailScreen from 'src/screen/MovieDetailScreen';

const MoviesScreen = () => {
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchMovies();
  }, []);
  const fetchMovies = () => {
    setLoading(true);
    axios
      .get('http://movietome.unomy.pw/movie')
      .then(response => {
        console.log(response);
        setMovies(response.data);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };
  return (
    <View style={styles.container}>
      {movies ? (
        <ScrollView
          contentContainerStyle={styles.scrollContent}
          // Hide all scroll indicators
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={fetchMovies} />
          }>
          {movies.map((movie, index) => (
            <MoviePoster movie={movie} key={index} />
          ))}
        </ScrollView>
      ) : (
        // <FlatList
        //   style={styles.scrollContent}
        //   data={movies ? movies : []}
        //   renderItem={({item}) => <MoviePoster movie={item} />}
        //   keyExtractor={(item, index) => index + ''}
        // />
        <ActivityIndicator
          animating={loading}
          style={styles.loader}
          size="large"
        />
      )}
    </View>
  );
};

export default MoviesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, // take up all screen
    // start below status bar
  },
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
  },
  scrollContent: {
    flexDirection: 'row', // arrange posters in rows
    flexWrap: 'wrap',
    marginTop : 10 // allow multiple rows
  },
});
