import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import {useRoute, useNavigation} from '@react-navigation/native';
import {connect} from 'react-redux';
const HistoryScreen = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState();
  const route = useRoute();
  const [data, setData] = useState();
  const {token} = route.params;
  useEffect(() => {
    fetchHistory();
  }, []);
  const fetchHistory = () => {
    setLoading(true);
    axios
      .get('http://movietome.unomy.pw/booking', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(response => {
        console.log(response.data);
        setData(response.data);
      })

      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };
  const RenderList = ({item}) => {
    const {movie} = item.showtime;

    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() =>
          navigation.navigate('HistoryDetail', {
            bookingId: item._id,
          })
        }>
        <ImageBackground
          source={{uri: movie.image}}
          style={{
            height: 130,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              backgroundColor: 'rgba( 0, 0, 0, 0.6 )',
              padding: 12,
            }}>
            <View style={{flex: 1}}>
              <Image
                source={{uri: movie.image}}
                style={{...StyleSheet.absoluteFillObject}}
              />
            </View>
            <View
              style={{
                flex: 3,
                padding: 10,
              }}>
              <Text style={{fontWeight: '700', color: 'white'}}>
                {movie.name}
              </Text>
              <Text style={{color: 'white'}}>{movie.duration} mins</Text>
              <Text style={{color: 'white'}}>Rating : {movie.rating}</Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  };
  return data ? (
    <View style={{flex: 1}}>
      <LinearGradient
        colors={['#ff9100', '#c8cc28']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={{
          height: 40,
          justifyContent: 'center',
          padding: 5,
          alignItems: 'center',
          elevation: 4,
        }}>
        <Text style={{fontWeight: '700', color: 'white', marginLeft: 15}}>
          Booking History
        </Text>
      </LinearGradient>

      <FlatList
        data={data ? data : []}
        renderItem={({item}) => <RenderList item={item} />}
        keyExtractor={(item, index) => index + ''}
      />
    </View>
  ) : (
    <ActivityIndicator
      animating={loading}
      style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
      size="large"
    />
  );
};

export default HistoryScreen;
