import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {logout} from 'src/action/authAction';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {login, fetchUser} from 'src/reducer/AuthReducer';
import axios from 'axios';
import {TextInput} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import {
  faTicketAlt,
  faArrowRight,
  faChevronRight,
  faSignOutAlt,
} from '@fortawesome/free-solid-svg-icons';
import LinearGradient from 'react-native-linear-gradient';
const MoreScreen = props => {
  const {isLogin, token} = props.auth;
  const {login, logout, fetchUser} = props;
  const [username, setusername] = useState('');
  const [password, setpassword] = useState('');
  const navigation = useNavigation();
  console.log(isLogin);

  useEffect(() => {
    if (isLogin === true) {
      fetchUser(token);
    }
  }, [isLogin]);

  const handleChangeInput = (key, text) => {
    if (key === 'username') {
      setusername(text);
    } else {
      setpassword(text);
    }
  };
  return (
    <LinearGradient
      colors={['#c7c7c7', '#fff']}
      start={{x: -1, y: 1}}
      end={{x: 0, y: 0}}
      style={{flex: 1}}>
      <View style={{alignItems: 'center'}}>
        <View
          style={{
            width: 100,
            height: 100,
            borderRadius: 50,

            backgroundColor: 'orange',
            marginTop: 40,
            marginBottom: 20,
          }}></View>
      </View>
      {isLogin ? (
        <View
          style={{
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('History', {
                token: token,
              })
            }
            style={{
              height: 40,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <FontAwesomeIcon icon={faTicketAlt} color="#807e7c" size={24} />
            </View>
            <View style={{flex: 5, justifyContent: 'center'}}>
              <Text style={{color: '#575553'}}>Booking History</Text>
            </View>
            <View style={{flex: 1, justifyContent: 'flex-end'}}>
              <FontAwesomeIcon
                icon={faChevronRight}
                color="#807e7c"
                size={24}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => logout(token)}
            style={{
              height: 40,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <FontAwesomeIcon icon={faSignOutAlt} color="#807e7c" size={24} />
            </View>
            <View style={{flex: 5, justifyContent: 'center'}}>
              <Text style={{color: '#575553'}}>Sign out</Text>
            </View>
            <View style={{flex: 1, justifyContent: 'flex-end'}}>
              <FontAwesomeIcon
                icon={faChevronRight}
                color="#807e7c"
                size={24}
              />
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={{alignItems: 'center'}}>
          <TextInput
            placeholder="Username"
            style={{
              padding: 5,
              height: 40,
              width: 200,
              borderColor: '#3d81d6',
              borderWidth: 1,
              borderRadius: 3,
              marginBottom: 10,
            }}
            onChangeText={text => handleChangeInput('username', text)}
            value={username}
          />
          <TextInput
            placeholder="Password"
            secureTextEntry
            style={{
              padding: 5,
              height: 40,
              width: 200,
              borderColor: 'gray',
              borderWidth: 1,
              borderColor: '#3d81d6',
              borderRadius: 3,
              marginBottom: 10,
            }}
            onChangeText={text => handleChangeInput('password', text)}
            value={password}
          />
          <TouchableOpacity
            onPress={() => login(username, password)}
            style={{
              width: 200,
              height: 40,
              margin: 5,
              padding: 5,
              justifyContent: 'center',
              alignItems: 'center',
              elevation: 4,
              borderRadius: 2,
            }}>
            <LinearGradient
              colors={['#2e87de', '#88ccf0']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              style={{
                borderRadius: 2,
                ...StyleSheet.absoluteFillObject,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white', fontWeight: '700'}}>SIGN IN</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      )}
    </LinearGradient>
  );
};
// const mapStateToProps = state => {
//   return {
//     auth: state.auth,
//   };
// };

// const mapDispatchToProps = dispatch => {
//   return bindActionCreators({login}, dispatch);
// };

// export default connect(mapStateToProps, mapDispatchToProps)(MoreScreen);
export default connect(
  ({auth}) => ({
    auth,
  }),
  {
    login,
    logout,
    fetchUser,
  },
)(MoreScreen);
