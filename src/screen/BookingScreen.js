import React, {useEffect, useState, useMemo} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Image,
  ImageBackground,
  ScrollView,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import Seat from 'src/component/Seat';
import {Button} from 'react-native-paper';
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import PinchZoomView from 'react-native-pinch-zoom-view';
import moment from 'moment';
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {
  faVolumeUp,
  faClosedCaptioning,
  faClock,
  faVideo,
  faMapMarkerAlt,
} from '@fortawesome/free-solid-svg-icons';

const BookingScreen = props => {
  const [data, setData] = useState();
  const route = useRoute();
  const navigation = useNavigation();
  const showTimeId = route.params.showtime;
  //console.log('movieId', movieId);
  const [loading, setLoading] = useState(false);
  const [onZoom, setOnZoom] = useState(false);
  const [selectedSeats, setSelectedSeats] = useState([]);
  const [price, setPrice] = useState();
  const {token, user} = props.auth;
  const [onToggle, setOntoggle] = useState();
  console.log('token', token);
  useEffect(() => {}, []);

  const toggleModal = () => {
    setOntoggle(!onToggle);
  };

  const fetchData = () => {
    setLoading(true);
    axios
      .get('http://movietome.unomy.pw/showtime/' + showTimeId)
      .then(response => {
        setData(response.data);
      })

      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };
  console.log('user', user);

  const onBooking = () => {
    const objectData = {
      showtimeId: showTimeId,
      seats: selectedSeats,
    };
    console.log('token', token);
    console.log(objectData);

    axios
      .post(
        'http://movietome.unomy.pw/booking',

        {
          showtimeId: showTimeId,
          seats: selectedSeats,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then(response => {
        console.log(response);
        navigation.navigate('Purchase', {
          purchaseData: response.data,
        });
      })

      .catch(error => {
        if (error.message === 'Request failed with status code 400') {
          alert('Please buy a Ticket atleast 1 hour before Showtime start');
        }
        //alert(error);
        console.log(JSON.stringify(error));
      });
  };

  console.log('data showtime', data);
  console.log('price', price);

  const result = useMemo(() => {
    if (data) {
      let resultarr = data.seats.map(seatType => {
        return {
          ...seatType,
          values: seatType.values.map((seatRow, row) => {
            return seatRow.map((seat, col) => {
              if (seat === null) {
                let isSelected = selectedSeats.find(selectingSeat => {
                  if (
                    selectingSeat.type === seatType.type &&
                    selectingSeat.row === row + 1 &&
                    selectingSeat.column === col + 1
                  ) {
                    return true;
                  }
                  return false;
                });
                if (isSelected) {
                  return 'selected';
                } else {
                  return null;
                }
              } else {
                return 'book';
              }
            });
          }),
        };
      });
      return resultarr;
    } else {
      return [];
    }
  }, [data, selectedSeats]);

  useEffect(() => {
    fetchData();
  }, []);
  useEffect(() => {
    if (data) {
      setPrice({
        normal: data.theater.seats.find(value => value.type === 'NORMAL'),
        deluxe: data.theater.seats.find(value => value.type === 'DELUXE'),
        sofa: data.theater.seats.find(value => value.type === 'SOFA'),
      });
    }
  }, [data]);

  const renderSeats = (arr, type) => {
    return arr.values.map((value, row) => {
      return (
        <View style={{flexDirection: 'row'}} key={row}>
          {value.map((seat, col) => (
            <Seat
              type={type}
              key={col}
              addSeat={addSeat}
              deleteSeat={deleteSeat}
              row={row}
              col={col}
              status={seat}
            />
          ))}
        </View>
      );
    });
  };

  const addSeat = (type, row, col) => {
    console.log('add seat!');
    if (type === 'SOFA') {
      if (col % 2 === 0) {
        setSelectedSeats([
          ...selectedSeats,
          {type, row: row + 1, column: col + 1},
          {type, row: row + 1, column: col + 2},
        ]);
      } else {
        setSelectedSeats([
          ...selectedSeats,
          {type, row, column: col},
          {type, row: row, column: col - 1},
        ]);
      }
    } else {
      setSelectedSeats([
        ...selectedSeats,
        {type, row: row + 1, column: col + 1},
      ]);
    }
  };

  const deleteSeat = (type, row, col) => {
    if (type === 'SOFA') {
      setSelectedSeats(
        selectedSeats.filter((value, index) => {
          if (value.type == type) {
            if (value.row == row) {
              if (value.column == col) {
              } else {
                return value;
              }
            } else {
              return value;
            }
          } else {
            return value;
          }
        }),
      );
    } else {
      setSelectedSeats(
        selectedSeats.filter((value, index) => {
          if (value.type == type) {
            if (value.row == row) {
              if (value.column == col) {
              } else {
                return value;
              }
            } else {
              return value;
            }
          } else {
            return value;
          }
        }),
      );
    }
  };

  const calculateTotal = () => {
    let total = 0;
    selectedSeats.forEach((value, index) => {
      switch (value.type) {
        case 'NORMAL':
          total += price.normal.price;
          break;
        case 'DELUXE':
          total += price.deluxe.price;
          break;
        case 'SOFA':
          total += price.sofa.price;
          break;
        default:
          total += 0;
          break;
      }
    });
    return total;
  };
  console.log('selected seat', selectedSeats);
  return data ? (
    <View style={{flex: 1}}>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 1,
          zIndex: 1,
        }}>
        <Text style={{fontWeight: '700'}}>{data.movie.name}</Text>
      </View>
      <View
        style={{
          height: 130,
          flexDirection: 'row',
          padding: 10,
          backgroundColor: '#fbfcfe',
        }}>
        <View
          style={{
            flex: 1,
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 10,
            backgroundColor: 'white',
          }}>
          <Image source={{uri: data.movie.image}} style={{flex: 1}} />
        </View>

        <View style={{flex: 3, paddingHorizontal: 10}}>
          <Text style={{fontSize: 12, fontWeight: 'bold'}}>
            {moment(data.startDateTime).format('DD MMMM YYYY | HH:mm')}{' '}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faVideo} size={12} />
            </View>
            <Text style={{fontSize: 12}}>{data.theater.name}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faMapMarkerAlt} size={12} />
            </View>
            <Text style={{fontSize: 12}}>{data.cinema.name}</Text>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faClosedCaptioning} size={12} />
            </View>

            <Text
              style={{
                fontSize: 12,
              }}>
              {data.subtitle} |
            </Text>
            <View style={{marginHorizontal: 5}}>
              <FontAwesomeIcon icon={faVolumeUp} size={12} />
            </View>

            <Text style={{fontSize: 12}}>{data.soundtrack}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faClock} size={12} />
            </View>
            <Text style={{fontSize: 12}}>{data.movie.duration} mins</Text>
          </View>
        </View>
      </View>
      <View
        style={{
          height: 40,
          backgroundColor: '#eef3f6',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{alignItems: 'center', marginRight: 10}}>
          <Image
            style={{
              width: 12,
              height: 12,
            }}
            source={require('src/assets/lounge_blue.png')}
          />
          <Text style={{fontSize: 7}}>Normal</Text>
          <Text style={{fontSize: 7}}>
            {price ? price.normal.price : null} THB
          </Text>
        </View>
        <View style={{alignItems: 'center', marginRight: 10}}>
          <Image
            style={{
              width: 12,
              height: 12,
            }}
            source={require('src/assets/lounge_red.png')}
          />
          <Text style={{fontSize: 7}}>Deluxe</Text>
          <Text style={{fontSize: 7}}>
            {price ? price.deluxe.price : null} THB
          </Text>
        </View>
        <View style={{alignItems: 'center', marginRight: 10}}>
          <Image
            style={{
              width: 12,
              height: 12,
            }}
            source={require('src/assets/lounge_green.png')}
          />
          <Text style={{fontSize: 7}}>Sofa</Text>
          <Text style={{fontSize: 7}}>
            {price ? price.sofa.price : null} THB
          </Text>
        </View>
      </View>

      <View
        style={{
          zIndex: -1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
          flex: 4,
        }}>
        <ReactNativeZoomableView
          maxZoom={1.5}
          minZoom={1}
          zoomStep={0.1}
          initialZoom={1}
          bindToBorders={true}
          onZoomBefore={() => setOnZoom(true)}
          onZoomEnd={() => setOnZoom(false)}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#fbfcfe',
          }}>
          <View
            style={{
              alignItems: 'center',
              marginBottom: 5,
              backgroundColor: 'lightgreen',
              width: 250,
            }}>
            <Text style={{fontSize: 10, fontWeight: 'bold'}}>Screen</Text>
          </View>
          <View style={{marginVertical: 10}}>
            {renderSeats(
              result.find(value => value.type === 'NORMAL'),
              'NORMAL',
            )}
          </View>
          <View
            style={{marginVertical: 10}}
            pointerEvents={onZoom ? 'none' : 'auto'}>
            {renderSeats(
              result.find(value => value.type === 'DELUXE'),
              'DELUXE',
            )}
          </View>
          <View style={{marginVertical: 10}}>
            {renderSeats(
              result.find(value => value.type === 'SOFA'),
              'SOFA',
            )}
          </View>
        </ReactNativeZoomableView>
      </View>
      <View
        style={{
          flex: 1.25,
          backgroundColor: 'white',
          elevation: 6,
          padding: 5,
          justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 1,
          }}>
          <Text style={{fontSize: 12, color: 'gray'}}>Selected Seats</Text>
          <ScrollView>
            <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
              {selectedSeats
                ? selectedSeats.map((value, index) => (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginRight: 2,
                      }}
                      key={index}>
                      {value.type === 'NORMAL' ? (
                        <Image
                          style={{
                            width: 12,
                            height: 12,
                          }}
                          source={require('src/assets/lounge_blue.png')}
                        />
                      ) : (
                        <Image
                          style={{
                            width: 12,
                            height: 12,
                          }}
                          source={
                            value.type === 'DELUXE'
                              ? require('src/assets/lounge_red.png')
                              : require('src/assets/lounge_green.png')
                          }
                        />
                      )}

                      <Text>{`(${value.row},${value.column})`}</Text>
                    </View>
                  ))
                : null}
            </View>
          </ScrollView>
        </View>

        <View style={{flex: 1, alignItems: 'flex-end', paddingRight: 10}}>
          <View style={{marginBottom: 10}}>
            <Text style={{fontSize: 12, color: 'gray'}}>Total</Text>
            <Text style={{fontWeight: '700', color: 'blue'}}>
              {calculateTotal()} THB
            </Text>
          </View>
          <Button
            mode="contained"
            style={{width: 120}}
            onPress={() => {
              setOntoggle(!onToggle);
              //onBooking();
            }}>
            CONTINUE
          </Button>
        </View>
      </View>
      <Modal isVisible={onToggle}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            borderRadius: 5,
            marginVertical: 100,
          }}>
          <View
            style={{
              height: 130,
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#fbfcfe',
              elevation: 2,
            }}>
            <View
              style={{
                flex: 1,
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 10,
                backgroundColor: 'white',
              }}>
              <Image source={{uri: data.movie.image}} style={{flex: 1}} />
            </View>

            <View
              style={{flex: 3, paddingHorizontal: 10}}
              customBackdrop={
                <TouchableWithoutFeedback
                  onPress={() => setOntoggle(!onToggle)}>
                  <View style={{flex: 1}} />
                </TouchableWithoutFeedback>
              }>
              <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                {moment(data.startDateTime).format('DD MMMM YYYY | HH:mm')}{' '}
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{marginRight: 5}}>
                  <FontAwesomeIcon icon={faVideo} size={12} />
                </View>
                <Text style={{fontSize: 12}}>{data.theater.name}</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{marginRight: 5}}>
                  <FontAwesomeIcon icon={faMapMarkerAlt} size={12} />
                </View>
                <Text style={{fontSize: 12}}>{data.cinema.name}</Text>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{marginRight: 5}}>
                  <FontAwesomeIcon icon={faClosedCaptioning} size={12} />
                </View>

                <Text
                  style={{
                    fontSize: 12,
                  }}>
                  {data.subtitle} |
                </Text>
                <View style={{marginHorizontal: 5}}>
                  <FontAwesomeIcon icon={faVolumeUp} size={12} />
                </View>

                <Text style={{fontSize: 12}}>{data.soundtrack}</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{marginRight: 5}}>
                  <FontAwesomeIcon icon={faClock} size={12} />
                </View>
                <Text style={{fontSize: 12}}>{data.movie.duration} mins</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',

              padding: 5,
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 1,
              }}>
              <Text style={{fontSize: 12, color: 'gray'}}>Selected Seats</Text>
              <ScrollView>
                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                  {selectedSeats
                    ? selectedSeats.map((value, index) => (
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginRight: 2,
                          }}
                          key={index}>
                          {value.type === 'NORMAL' ? (
                            <Image
                              style={{
                                width: 12,
                                height: 12,
                              }}
                              source={require('src/assets/lounge_blue.png')}
                            />
                          ) : (
                            <Image
                              style={{
                                width: 12,
                                height: 12,
                              }}
                              source={
                                value.type === 'DELUXE'
                                  ? require('src/assets/lounge_red.png')
                                  : require('src/assets/lounge_green.png')
                              }
                            />
                          )}

                          <Text>{`(${value.row},${value.column})`}</Text>
                        </View>
                      ))
                    : null}
                </View>
              </ScrollView>
            </View>

            <View style={{flex: 1, alignItems: 'flex-end', paddingRight: 10}}>
              <View style={{marginBottom: 10}}>
                <Text style={{fontSize: 12, color: 'gray'}}>Total</Text>
                <Text style={{fontWeight: '700', color: 'blue'}}>
                  {calculateTotal()} THB
                </Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', margin: 10}}>
            <Button
              style={{flex: 1, margin: 5}}
              mode="outlined"
              onPress={() => setOntoggle(!onToggle)}>
              cancel
            </Button>
            <Button
              style={{flex: 1, margin: 5}}
              mode="contained"
              color="green"
              onPress={() => {
                onBooking();
                setOntoggle(!onToggle);
              }}>
              Purchase
            </Button>
          </View>
        </View>
      </Modal>
    </View>
  ) : (
    <ActivityIndicator animating={loading} style={styles.loader} size="large" />
  );
};

export default connect(({auth}) => ({
  auth,
}))(BookingScreen);

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
  },
});
