import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import moment from 'moment';
import axios from 'axios';
import {Button} from 'react-native-paper';
import {useRoute} from '@react-navigation/native';
import {Container, Header, Tab, Tabs, ScrollableTab} from 'native-base';
import ShowTime from 'src/component/ShowTime';
import {
  ScrollableTabView,
  ScrollableTabBar,
} from '@valdio/react-native-scrollable-tabview';
import LinearGradient from 'react-native-linear-gradient';
//import ScrollableTabView from 'react-native-scrollable-tab-view';

const MovieDetailScreen = props => {
  const route = useRoute();
  const {name, image, duration, description, rating, _id} = route.params.movie;
  const [cinemaList, setCinemaList] = useState();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const currentDate = new Date().getTime();

  const days = [
    moment().format(),
    moment()
      .add(1, 'days')
      .format(),
    moment()
      .add(2, 'days')
      .format(),
    moment()
      .add(3, 'days')
      .format(),
    moment()
      .add(4, 'days')
      .format(),
    moment()
      .add(5, 'days')
      .format(),
    moment()
      .add(6, 'days')
      .format(),
    moment()
      .add(7, 'days')
      .format(),
  ];

  useEffect(() => {
    fetchShowTime();
    //console.log(days);
  }, []);

  const fetchShowTime = () => {
    setLoading(true);
    axios
      .get(
        'http://movietome.unomy.pw/showtime/movie/' +
          _id +
          '?date=' +
          currentDate,
      )
      .then(response => {
        console.log(response);
        setData(response.data);
        const List = [...new Set(response.data.map(value => value.cinemaId))];
        //console.log('cinema List ', List);
        setCinemaList(List);
      })

      .catch(error => {
        setLoading(false);
        console.log(error);
      })
      .then(() => {
        setLoading(false);
      });
  };

  const RenderShowTime = () => {
    return days.map((value, index) => {
      if (index === 0) {
        return (
          <ShowTime heading="Today" day={value} movieId={_id} key={index} />
        );
      } else if (index === 1) {
        return (
          <ShowTime heading="Tomorrow" day={value} movieId={_id} key={index} />
        );
      } else {
        return (
          <ShowTime
            heading={moment(value).format('ddd D')}
            day={value}
            movieId={_id}
            key={index}
          />
        );
      }
    });
  };
  return (
    <>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 1,
          zIndex: 1,
        }}>
        <Text style={{fontWeight: '700', fontFamily: 'BaiJamajuree-Bold'}}>
          Select Showtime
        </Text>
      </View>

      <ImageBackground
        source={{uri: image}}
        style={{
          height: 130,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: 'rgba( 0, 0, 0, 0.6 )',
            padding: 12,
          }}>
          <View style={{flex: 1}}>
            <Image
              source={{uri: image}}
              style={{...StyleSheet.absoluteFillObject}}
            />
          </View>
          <View
            style={{
              flex: 3,
              padding: 10,
            }}>
            <Text style={{fontWeight: '700', color: 'white'}}>{name}</Text>
            <Text style={{color: 'white'}}>{duration} mins</Text>
            <Text style={{color: 'white'}}>Rating : {rating}</Text>
          </View>
        </View>
      </ImageBackground>
      {data ? (
        <Tabs
          tabBarBackgroundColor="white"
          tabBarActiveTextColor="#3d81d6"
          tabBarTextStyle={{fontSize: 13}}
          tabBarUnderlineStyle={{backgroundColor: '#3d81d6'}}
          renderTabBar={() => <ScrollableTabBar />}>
          {RenderShowTime()}
        </Tabs>
      ) : (
        <ActivityIndicator
          animating={loading}
          style={styles.loader}
          size="large"
        />
      )}
    </>
  );
};

export default MovieDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, // take up all screen
    paddingTop: 20, // start below status bar
  },
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
  },
  scrollContent: {
    flexDirection: 'row', // arrange posters in rows
    flexWrap: 'wrap', // allow multiple rows
  },
});
