import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  ImageBackground,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Container, Header, Tab, Tabs, ScrollableTab} from 'native-base';
import moment from 'moment';
import {
  ScrollableTabView,
  ScrollableTabBar,
} from '@valdio/react-native-scrollable-tabview';
import axios from 'axios';
import {useRoute} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import CinemaShowTIme from '../component/CinemaShowTIme';

const CinemaDetailScreen = () => {
  const [cinema, setCinema] = useState();
  const [showTime, setShowtime] = useState();
  const [loading, setLoading] = useState();
  const [loadingShowTime, setLoadingShowTime] = useState();
  const [showtimeByMovie, setshowtimeByMovie] = useState();
  const [movieList, setMovieList] = useState();
  const navigation = useNavigation();
  //const [target, setTarget] = useState({});
  const route = useRoute();
  const {cinemaId} = route.params;
  useEffect(() => {
    fetchCinema();
  }, []);

  const days = [
    moment().format(),
    moment()
      .add(1, 'days')
      .format(),
    moment()
      .add(2, 'days')
      .format(),
    moment()
      .add(3, 'days')
      .format(),
    moment()
      .add(4, 'days')
      .format(),
    moment()
      .add(5, 'days')
      .format(),
    moment()
      .add(6, 'days')
      .format(),
    moment()
      .add(7, 'days')
      .format(),
  ];

  const fetchCinema = () => {
    setLoadingShowTime(true);
    axios
      .get('http://movietome.unomy.pw/cinema/' + cinemaId)
      .then(response => {
        //console.log(response);
        setCinema(response.data);
      })

      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };

  return cinema ? (
    <View style={{flex: 1}}>
      <View
        style={{
          backgroundColor: 'white',
          height: 40,
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 1,
          zIndex: 1,
        }}>
        <Text style={{fontWeight: '700'}}>Select Showtime</Text>
      </View>
      <LinearGradient
        colors={['#ff9100', '#c8cc28']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={{
          height: 40,
          justifyContent: 'center',
          padding: 5,
        }}>
        <Text style={{fontWeight: '700', color: 'white', marginLeft: 15}}>
          {cinema.name}
        </Text>
      </LinearGradient>

      <ScrollableTabView
        tabBarBackgroundColor="white"
        tabBarActiveTextColor="#3d81d6"
        tabBarTextStyle={{fontSize: 13}}
        tabBarUnderlineStyle={{backgroundColor: '#3d81d6'}}
        renderTabBar={() => <ScrollableTabBar />}>
        {days.map((day, index) => (
          <CinemaShowTIme
            tabLabel={moment(day).format('ddd D')}
            day={day}
            cinemaId={cinemaId}
            key={index}
          />
        ))}
      </ScrollableTabView>
    </View>
  ) : (
    <ActivityIndicator
      animating={loadingShowTime}
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}
      size="large"
    />
  );
};

export default CinemaDetailScreen;
