import React, {useEffect, useState} from 'react';
import {View, Text, Image} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import {connect} from 'react-redux';
import QRCode from 'react-native-qrcode-svg';
import LinearGradient from 'react-native-linear-gradient';
const HistoryDetailScreen = props => {
  const navigation = useNavigation();
  const {token} = props.auth;
  const route = useRoute();
  const {bookingId} = route.params;
  const [data, setData] = useState();
  const [loading, setLoading] = useState();
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = () => {
    setLoading(true);
    axios
      .get('http://movietome.unomy.pw/booking/' + bookingId, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(response => {
        console.log(response.data);
        setData(response.data);
      })

      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <LinearGradient
      colors={['#2e87de', '#88ccf0']}
      start={{x: -1, y: 1}}
      end={{x: 0, y: 0}}
      style={{flex: 1}}>
      <View
        style={{
          height: 40,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 1,
        }}>
        <Text style={{fontWeight: 'bold'}}>Booking Histiory</Text>
      </View>
      <View
        style={{
          backgroundColor: 'white',
          marginHorizontal: 30,
          marginVertical: 50,
          flex: 1,
          elevation: 5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <Image
            source={require('src/assets/sfx.png')}
            style={{height: 35, width: 75}}
          />
          <View style={{marginTop: 20}}>
            <QRCode value={data._id} size={100} />
          </View>

          <Text style={{marginTop: 10, fontWeight: 'bold'}}>Booking id </Text>
          <Text>{data._id}</Text>
        </View>
        <LinearGradient
          colors={['#ad8e4a', '#faf6b6']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          style={{
            height: 30,
            backgroundColor: 'lightgreen',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>Paid</Text>
        </LinearGradient>
        <View style={{flex: 2}}></View>
      </View>
    </LinearGradient>
  );
};

export default connect(({auth}) => ({
  auth,
}))(HistoryDetailScreen);
