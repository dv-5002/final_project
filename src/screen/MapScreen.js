import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

const MapScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={styles.text}>Hello world</Text>
    </View>
  );
};

export default MapScreen;
export const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontFamily: 'roboto',
    fontWeight : 'bold'
  },
});
