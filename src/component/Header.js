import React from 'react';
import {View, Text, Image} from 'react-native';

const HeaderLogo = () => {
  return (
    <View
      style={{
        height: 40,
        backgroundColor: 'white',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
        elevation: 1,
      }}>
      <Image
        source={require('src/assets/Sf-ipad.png')}
        style={{width: 50, height: 20}}
      />
    </View>
  );
};

export default HeaderLogo;
