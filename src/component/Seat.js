import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheckCircle} from '@fortawesome/free-solid-svg-icons';
const Seat = ({type, addSeat, deleteSeat, status, row, col}) => {
  switch (type) {
    case 'NORMAL':
      return status === 'book' ? (
        <Image
          style={{
            width: 12,
            height: 12,
          }}
          source={require('src/assets/lounge_black.png')}
        />
      ) : (
        <TouchableOpacity
          onPress={() => {
            status === 'selected'
              ? deleteSeat('NORMAL', row, col)
              : addSeat('NORMAL', row, col);
          }}>
          {status === 'selected' ? (
            <FontAwesomeIcon
              icon={faCheckCircle}
              color="lightgreen"
              size={12}
            />
          ) : (
            <Image
              style={{
                width: 12,
                height: 12,
              }}
              source={require('src/assets/lounge_blue.png')}
            />
          )}
        </TouchableOpacity>
      );

    case 'DELUXE':
      return status === 'book' ? (
        <Image
          style={{
            width: 12,
            height: 12,
          }}
          source={require('src/assets/lounge_black.png')}
        />
      ) : (
        <TouchableOpacity
          onPress={() => {
            status === 'selected'
              ? deleteSeat('DELUXE', row, col)
              : addSeat('DELUXE', row, col);
          }}>
          {status === 'selected' ? (
            <FontAwesomeIcon
              icon={faCheckCircle}
              color="lightgreen"
              size={12}
            />
          ) : (
            <Image
              style={{
                width: 12,
                height: 12,
              }}
              source={require('src/assets/lounge_red.png')}
            />
          )}
        </TouchableOpacity>
      );

    case 'SOFA':
      return status === 'book' ? (
        <Image
          style={{
            width: 12,
            height: 12,
            marginBottom: 5,
            flexDirection: 'row',
            marginLeft: col % 2 === 0 ? 10 : 1,
          }}
          source={require('src/assets/lounge_black.png')}
        />
      ) : (
        <TouchableOpacity
          onPress={() => {
            status === 'selected'
              ? deleteSeat('SOFA', row, col)
              : addSeat('SOFA', row, col);
          }}>
          {status === 'selected' ? (
            <FontAwesomeIcon
              icon={faCheckCircle}
              style={{marginBottom: 5, marginLeft: col % 2 === 0 ? 12 : 1}}
              color="green"
              size={12}
            />
          ) : (
            <Image
              style={{
                width: 12,
                height: 12,
                marginBottom: 5,
                marginLeft: col % 2 === 0 ? 12 : 1,
              }}
              source={require('src/assets/lounge_green.png')}
            />
          )}
        </TouchableOpacity>
      );

    default:
      return null;
  }
};

export default Seat;
