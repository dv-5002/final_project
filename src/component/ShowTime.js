import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Tab} from 'native-base';
import moment from 'moment';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import {Button} from 'react-native-paper';
import {TouchableOpacity} from 'react-native-gesture-handler';
const ShowTime = props => {
  const navigation = useNavigation();
  const [cinemaList, setCinemaList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const {day, movieId} = props;

  useEffect(() => {
    fetchData();
  }, []);
  //console.log(movieId);
  console.log(new Date(day).getTime());

  const fetchData = () => {
    setLoading(true);
    axios
      .get(
        'http://movietome.unomy.pw/showtime/movie/' +
          movieId +
          '?date=' +
          new Date(day).getTime(),
      )
      .then(response => {
        console.log(response);
        setData(response.data);
        const List = [...new Set(response.data.map(value => value.cinemaId))];
        //console.log('cinema List ', List);
        setCinemaList(List);
      })

      .catch(error => {
        setLoading(false);
        console.log(error);
      })
      .then(() => {
        setLoading(false);
      });
  };
  const RenderCard = ({cinemaId}) => {
    const showtime = data.filter(value => value.cinemaId === cinemaId);
    const upcoming = showtime.filter((value, index) => {
      return new Date(value.startDateTime) > new Date();
    });
    const sortDate = upcoming.sort(
      (a, b) => new Date(a.startDateTime) - new Date(b.startDateTime),
    );
    const theaterList = [...new Set(sortDate.map(value => value.theater.id))];

    if (sortDate.length > 0) {
      return (
        <View
          style={{
            height: null,
            flexDirection: 'row',
            backgroundColor: '#fafafa',
          }}>
          <View style={{flex: 1}}>
            <View
              style={{
                backgroundColor: '#dfe5f1',
                height: 40,
                justifyContent: 'center',
                padding: 5,
              }}>
              <Text
                style={{fontWeight: '700', color: '#446eb8', marginLeft: 15}}>
                {showtime[0].cinema.name}
              </Text>
            </View>
            {theaterList.map((theaterId, index) => {
              const showtimeButton = sortDate.filter(
                value => value.theaterId === theaterId,
              );

              return (
                <View
                  key={index}
                  style={{
                    // alignItems: 'center',
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                  }}>
                  <Text style={{fontWeight: '700', color: '#494949'}}>
                    {showtimeButton[0].theater.name}
                  </Text>
                  <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                    {showtimeButton.map((value, index) => {
                      const time = new Date(value.startDateTime);
                      if (index === 0) {
                        return (
                          <TouchableOpacity
                            style={{
                              width: 80,
                              height: 40,
                              margin: 5,
                              backgroundColor: '#3d81d6',
                              padding: 5,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: 1,
                              elevation: 4,
                            }}
                            onPress={() =>
                              navigation.navigate('Booking', {
                                showtime: value._id,
                              })
                            }
                            key={index}>
                            <Text style={{color: 'white', fontWeight: '700'}}>
                              {time.getHours() +
                                ':' +
                                (time.getMinutes() < 10 ? '0' : '') +
                                time.getMinutes()}
                            </Text>
                          </TouchableOpacity>
                        );
                      } else {
                        return (
                          <TouchableOpacity
                            style={{
                              width: 80,
                              margin: 5,
                              height: 40,
                              padding: 5,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: 1,
                              backgroundColor: 'white',
                              elevation: 4,
                              borderWidth: 1,
                              borderColor: '#3d81d6',
                            }}
                            onPress={() =>
                              navigation.navigate('Booking', {
                                showtime: value._id,
                              })
                            }
                            key={index}>
                            <Text>
                              {time.getHours() +
                                ':' +
                                (time.getMinutes() < 10 ? '0' : '') +
                                time.getMinutes()}
                            </Text>
                          </TouchableOpacity>
                        );
                      }
                    })}
                  </View>
                </View>
              );
            })}
          </View>
        </View>
      );
    } else {
      return null;
    }
  };
  return loading ? (
    <ActivityIndicator animating={loading} style={styles.loader} size="large" />
  ) : (
    <Tab>
      {cinemaList.length == 0 ? (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <Text>No Movie Showtime</Text>
        </View>
      ) : (
        <FlatList
          data={cinemaList ? cinemaList : []}
          renderItem={({item}) => <RenderCard cinemaId={item} />}
          keyExtractor={(item, index) => index + ''}
          
        />
      )}
    </Tab>
  );
};

export default ShowTime;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
  },
});
