import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
const {width, height} = Dimensions.get('window');
const cols = 3;
const rows = 3;
const MoviePoster = props => {
  const {name, image, duration, _id} = props.movie;
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={() => {
        navigation.navigate('MovieDetail', {
          movie: props.movie,
        });
      }}>
      <View
        style={{
          flex: 1,

          elevation: 20,
          backgroundColor: 'white',
        }}>
        <Image source={{uri: image}} style={{flex: 1}} />
      </View>
      <View style={{alignItems: 'center'}}>
        <Text style={styles.title} numberOfLines={1}>
          {name}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default MoviePoster;

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginBottom: 10,
    height: (height - 20 - 20) / rows - 10,
    width: (width - 10) / cols - 10,
    elevation: 4,
  },
  imageContainer: {
    flex: 1, // take up all available space
    elevation: 4,
  },
  image: {
    // rounded corners
    ...StyleSheet.absoluteFillObject, // fill up all space in a container
  },
  title: {
    // ...defaultStyles.text,
    fontSize: 12,
    fontWeight : '700',
    marginTop: 4,
  },
  genre: {
    // ...defaultStyles.text,
    color: '#BBBBBB',
    fontSize: 12,
    lineHeight: 14,
  },
});
