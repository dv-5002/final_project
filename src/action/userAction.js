export const REGISTER_BEGIN = 'REGISTER_BEGIN';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';


export const registerBegin = () => ({
  type: REGISTER_BEGIN,
});

export const registerSuccess = response => ({
  type: REGISTER_SUCCESS,
  payLoad: response,
});
export const registerError = error => ({
  type: REGISTER_ERROR,
  payLoad: error,
});

