export const CHECK_LOGIN_BEGIN = 'CHECK_LOGIN_BEGIN';
export const CHECK_LOGIN_SUCCESS = 'CHECK_LOGIN_SUCCESS';
export const CHECK_LOGIN_ERROR = 'CHECK_LOGIN_ERROR';
export const LOGIN_BEGIN = 'LOGIN_BEGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGOUT = 'LOGOUT';
export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCESS';
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR';
export const REGISTER_BEGIN = 'REGISTER_BEGIN';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';
export const UPDATE_BEGIN = 'UPDATE_BEGIN';
export const UPDATE_SUCCESS = 'UPDATE_SUCCESS';
export const UPDATE_ERROR = 'UPDATE_ERROR';

export const loginBegin = () => ({
  type: LOGIN_BEGIN,
});

export const loginSuccess = token => ({
  type: LOGIN_SUCCESS,
  payLoad: token,
});

export const loginError = error => ({
  type: LOGIN_ERROR,
  payLoad: error,
});

export const logout = token => ({type: LOGOUT, payLoad: token});

export const checkAsyncStorageBegin = () => ({
  type: CHECK_LOGIN_BEGIN,
});
export const checkAsyncStorageSuccess = data => ({
  type: CHECK_LOGIN_SUCCESS,
  payLoad: data,
});

export const checkAsyncStorageError = err => ({
  type: CHECK_LOGIN_ERROR,
  payLoad: err,
});

export const fetchUserActionBegin = () => ({
  type: FETCH_USER_BEGIN,
});

export const fetchUserActionSuccess = data => ({
  type: FETCH_USER_SUCCESS,
  payLoad: data,
});
export const fetchUserActionError = err => ({
  type: FETCH_USER_ERROR,
  payLoad: err,
});

export const updateBegin = () => ({
  type: UPDATE_BEGIN,
});

export const updateSuccess = response => ({
  type: UPDATE_SUCCESS,
  payLoad: response,
});
export const updateError = error => ({
  type: UPDATE_ERROR,
  payLoad: error,
});

export const registerBegin = () => ({
  type: REGISTER_BEGIN,
});

export const registerSuccess = response => ({
  type: REGISTER_SUCCESS,
  payLoad: response,
});
export const registerError = error => ({
  type: REGISTER_ERROR,
  payLoad: error,
});
