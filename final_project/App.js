import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import StackRouter from './src/router/StackRouter';
import { withTheme, DefaultTheme, DarkTheme, Provider as PaperProvider } from 'react-native-paper'
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { rootReducer } from './src/reducers/index';
import { PersistGate } from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';

const App = () => {
  const enhancer = compose(
    applyMiddleware(thunk),
    //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    // other store enhancers if any
  );
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
  };
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, enhancer);
  let persistor = persistStore(store);

  const theme = {
    ...DarkTheme,
    roundness: 10,
    dark: true,
    colors: {
      ...DarkTheme.colors,
      primary: '#9F3535',
      OnBackground: 'white',
      OnSurface: 'white'
    }
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider theme={theme}>
          <NavigationContainer>
            <StackRouter />
          </NavigationContainer>
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
