import axios from 'axios';
export const LOGIN_BEGIN = 'LOGIN_BEGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const login = (username, password) => {
  console.log('loginProcess', username, password);
  () => ({type: LOGIN_BEGIN});
  axios
    .post('http://movietome.unomy.pw/auth/login', {username, password})
    .then(response => {
      console.log('retrived Token', response.data.token);
      () => ({type: LOGIN_BEGIN_SUCCESS, payLoad: response.data.token});
    })
    .catch(error => {
      if (error.message === 'Network Error') {
        alert('Network error , please check your internet connection!');
      } else if (error.response.data.messages.includes('incorrect/username')) {
        alert('incorrect username');
        console.log(JSON.stringify(error));
      } else if (error.response.data.messages.includes('incorrect/password')) {
        alert('incorrect password');
      } else {
        alert(error.response.data.messages);
      }
      console.log(JSON.stringify(error));
      {
        () => ({type: LOGIN_BEGIN_ERROR, payLoad: error});
      }
    });
};
