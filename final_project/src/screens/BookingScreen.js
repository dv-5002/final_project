import React, { useEffect, useState, useMemo } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Image,
  ImageBackground,
  ScrollView,
} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Svg, Polyline } from 'react-native-svg';
import Seat from 'src/components/Seat';
import { Button, Text, Appbar } from 'react-native-paper';
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import PinchZoomView from 'react-native-pinch-zoom-view';
import moment from 'moment';
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {
  faVolumeUp,
  faClosedCaptioning,
  faClock,
  faVideo,
  faMapMarkerAlt,
} from '@fortawesome/free-solid-svg-icons';

const BookingScreen = props => {
  const [data, setData] = useState();
  const route = useRoute();
  const showTimeId = route.params.showtime;
  //console.log('movieId', movieId);
  const [loading, setLoading] = useState(false);
  const [onZoom, setOnZoom] = useState(false);
  const [selectedSeats, setSelectedSeats] = useState([]);
  const [price, setPrice] = useState();

  useEffect(() => { }, []);
  const fetchData = () => {
    setLoading(true);
    axios
      .get('http://movietome.unomy.pw/showtime/' + showTimeId)
      .then(response => {
        setData(response.data);
      })

      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };
  console.log('data showtime', data);
  console.log('price', price);
  const result = useMemo(() => {
    if (data) {
      let resultarr = data.seats.map(seatType => {
        return {
          ...seatType,
          values: seatType.values.map((seatRow, row) => {
            return seatRow.map((seat, col) => {
              if (seat === null) {
                let isSelected = selectedSeats.find(selectingSeat => {
                  if (
                    selectingSeat.type === seatType.type &&
                    selectingSeat.row === row &&
                    selectingSeat.col === col
                  ) {
                    return true;
                  }
                  return false;
                });
                if (isSelected) {
                  return 'selected';
                } else {
                  return null;
                }
              } else {
                return 'book';
              }
            });
          }),
        };
      });
      return resultarr;
    } else {
      return [];
    }
  }, [data, selectedSeats]);

  useEffect(() => {
    fetchData();
  }, []);
  useEffect(() => {
    if (data) {
      setPrice({
        normal: data.theater.seats.find(value => value.type === 'NORMAL'),
        deluxe: data.theater.seats.find(value => value.type === 'DELUXE'),
        sofa: data.theater.seats.find(value => value.type === 'SOFA'),
      });
    }
  }, [data]);

  const renderSeats = (arr, type) => {
    return arr.values.map((value, row) => {
      return (
        <View style={{ flexDirection: 'row' }} key={row}>
          {value.map((seat, col) => (
            <Seat
              type={type}
              key={col}
              addSeat={addSeat}
              deleteSeat={deleteSeat}
              row={row}
              col={col}
              status={seat}
            />
          ))}
        </View>
      );
    });
  };

  const addSeat = (type, row, col) => {
    console.log('add seat!');
    if (type === 'SOFA') {
      if (col % 2 === 0) {
        setSelectedSeats([
          ...selectedSeats,
          { type, row, col },
          { type, row: row, col: col + 1 },
        ]);
      } else {
        setSelectedSeats([
          ...selectedSeats,
          { type, row, col },
          { type, row: row, col: col - 1 },
        ]);
      }
    } else {
      setSelectedSeats([...selectedSeats, { type, row, col }]);
    }
  };

  const deleteSeat = (type, row, col) => {
    if (type === 'SOFA') {
      setSelectedSeats(
        selectedSeats.filter((value, index) => {
          if (value.type == type) {
            if (value.row == row) {
              if (value.col == col) {
              } else {
                return value;
              }
            } else {
              return value;
            }
          } else {
            return value;
          }
        }),
      );
    } else {
      setSelectedSeats(
        selectedSeats.filter((value, index) => {
          if (value.type == type) {
            if (value.row == row) {
              if (value.col == col) {
              } else {
                return value;
              }
            } else {
              return value;
            }
          } else {
            return value;
          }
        }),
      );
    }
  };

  const calculateTotal = () => {
    let total = 0;
    selectedSeats.forEach((value, index) => {
      switch (value.type) {
        case 'NORMAL':
          total += price.normal.price;
          break;
        case 'DELUXE':
          total += price.deluxe.price;
          break;
        case 'SOFA':
          total += price.sofa.price;
          break;
        default:
          total += 0;
          break;
      }
    });
    return total;
  };
  console.log('selected seat', selectedSeats);
  return data ? (
    <View style={{ flex: 1 }}>
      <Appbar
        style={{
          height: 40,
          alignItems: 'center',
          paddingTop: 50,
          paddingBottom: 30,
          justifyContent: 'center',
        }}>
        <Appbar.Content
          style={{ alignItems: 'center' }}
          title={data.movie.name}
        />
      </Appbar>
      <View
        style={{
          height: 130,
          flexDirection: 'row',
          padding: 10,
          backgroundColor: '#1a1b1f',
        }}>
        <View
          style={{
            flex: 1,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 10,
          }}>
          <Image source={{ uri: data.movie.image }} style={{ flex: 1 }} />
        </View>

        <View style={{ flex: 3, paddingHorizontal: 10 }}>
          <Text style={{ fontSize: 12, fontWeight: 'bold' }}>
            {moment(data.startDateTime).format('DD MMMM YYYY | HH:mm')}{' '}
          </Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: 5 }}>
              <FontAwesomeIcon icon={faVideo} size={12} style={{ color: '#BC3B3B' }} />
            </View>
            <Text style={{ fontSize: 12 }}>{data.theater.name}</Text>
          </View>

          <Text style={{ fontSize: 12 }}>{data.cinema.name}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: 5 }}>
              <FontAwesomeIcon icon={faClosedCaptioning} size={12} style={{ color: '#BC3B3B' }} />
            </View>

            <Text
              style={{
                fontSize: 12,
              }}>
              {data.subtitle} |
            </Text>
            <View style={{ marginHorizontal: 5 }}>
              <FontAwesomeIcon icon={faVolumeUp} size={12} style={{ color: '#BC3B3B' }} />
            </View>

            <Text style={{ fontSize: 12 }}>{data.soundtrack}</Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: 5 }}>
              <FontAwesomeIcon icon={faClock} size={12} style={{ color: '#BC3B3B' }} />
            </View>
            <Text style={{ fontSize: 12 }}>{data.movie.duration} mins</Text>
          </View>
        </View>
      </View>
      <View style={{ backgroundColor: '#202020', flexDirection: 'row', justifyContent: 'center' }}>
        <View style={{ flexDirection: 'column', alignItems: 'center', flex: 1 }}>
          <Image
            source={require('src/assets/normal-seat.png')}
            style={{ width: 30, height: 30 }}
          />
          <Text>Normal</Text>
          <Text>{price ? price.normal.price : null} Bath</Text>
        </View>
        <View style={{ flexDirection: 'column', alignItems: 'center', flex: 1 }}>
          <Image
            source={require('src/assets/deluxe-seat.png')}
            style={{ width: 30, height: 30 }}
          />
          <Text>Deluxe</Text>
          <Text>{price ? price.deluxe.price : null} Bath</Text>
        </View>
        <View style={{ flexDirection: 'column', alignItems: 'center', flex: 1 }}>
          <Image
            source={require('src/assets/sofa-seat.png')}
            style={{ width: 30, height: 30 }}
          />
          <Text>Sofa</Text>
          <Text>{price ? price.sofa.price : null} Bath</Text>
        </View>
      </View>

      <View
        style={{
          zIndex: -1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
          flex: 4,
        }}>
        <ReactNativeZoomableView
          maxZoom={1.5}
          minZoom={1}
          zoomStep={0.1}
          initialZoom={1}
          bindToBorders={true}
          onZoomBefore={() => setOnZoom(true)}
          onZoomEnd={() => setOnZoom(false)}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#1a1b1f',
          }}>
          <Image
            source={require('src/assets/curve.png')}
            style={{ width: 300, height: 40 }}
          />
          <Text style={{ fontSize: 10, fontWeight: 'bold' }}>Screen</Text>
          <View style={{ marginVertical: 10 }}>
            {renderSeats(
              result.find(value => value.type === 'NORMAL'),
              'NORMAL',
            )}
          </View>
          <View
            style={{ marginVertical: 10 }}
            pointerEvents={onZoom ? 'none' : 'auto'}>
            {renderSeats(
              result.find(value => value.type === 'DELUXE'),
              'DELUXE',
            )}
          </View>
          <View style={{ marginVertical: 10 }}>
            {renderSeats(
              result.find(value => value.type === 'SOFA'),
              'SOFA',
            )}
          </View>
        </ReactNativeZoomableView>
      </View>
      <View
        style={{
          flex: 1.25,
          backgroundColor: '#202020',
          elevation: 6,
          padding: 5,
          justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 1,
          }}>
          <Text style={{ fontSize: 12, color: 'gray' }}>Selected Seats</Text>
          <ScrollView>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
              {selectedSeats
                ? selectedSeats.map((value, index) => (
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 2 }}>
                    {value.type === 'NORMAL' ? (
                      <Image
                        source={require('src/assets/normal-seat.png')}
                        style={{ width: 15, height: 15 }} />
                    ) : (
                        <Image
                          source={{ uri: value.type === 'selected' ? 'https://img.icons8.com/dusk/600/000000/armchair.png' : 'https://img.icons8.com/dusk/600/000000/sofa.png' }}
                          style={{ width: 15, height: 15 }} />
                      )}

                    <Text>{`(${value.row},${value.col})`}</Text>
                  </View>
                ))
                : null}
            </View>
          </ScrollView>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-end', paddingRight: 10 }}>
          <View style={{ marginBottom: 10 }}>
            <Text style={{ fontSize: 15, color: 'gray' }}>Total</Text>
            <Text style={{ fontWeight: '700' }}>
              {calculateTotal()} THB
            </Text>
          </View>
          <Button
            mode="contained"
            style={{ width: 120 }}
            onPress={() => console.log('Pressed')}>
            CONTINUE
          </Button>
        </View>
      </View>

      {/* <PinchZoomView
      minScale={1}
        style={{
          zIndex: -1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{marginVertical: 10}}>
          {renderSeats(Normal, 'NORMAL')}
        </View>
        <View
          style={{marginVertical: 10}}
          pointerEvents={onZoom ? 'none' : 'auto'}>
          {renderSeats(Deluxe, 'DELUXE')}
        </View>
        <View style={{marginVertical: 10}}>{renderSeats(Sofa, 'SOFA')}</View>
      </PinchZoomView> */}
    </View>
  ) : (
      <ActivityIndicator animating={loading} style={styles.loader} size="large" />
    );
};
export default BookingScreen;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
  },
});
