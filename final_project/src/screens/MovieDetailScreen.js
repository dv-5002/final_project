import React, { useEffect, useState } from 'react';
import { View, ActivityIndicator, FlatList, StyleSheet, Image, ScrollView, ImageBackground, } from 'react-native';
import moment from 'moment';
import axios from 'axios';
import { Text, Appbar } from 'react-native-paper';
import { useRoute } from '@react-navigation/native';
import { Container, Header, Tabs, ScrollableTab } from 'native-base';
import ShowTime from 'src/components/ShowTime';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const MovieDetailScreen = props => {
  const route = useRoute();
  const { name, image, duration, description, rating, _id } = props.route.params.item;
  const [cinemaList, setCinemaList] = useState();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const currentDate = new Date().getTime();
  const Tab = createMaterialTopTabNavigator();
  const days = [
    moment().format(),
    moment()
      .add(1, 'days')
      .format(),
    moment()
      .add(2, 'days')
      .format(),
    moment()
      .add(3, 'days')
      .format(),
    moment()
      .add(4, 'days')
      .format(),
    moment()
      .add(5, 'days')
      .format(),
    moment()
      .add(6, 'days')
      .format(),
    moment()
      .add(7, 'days')
      .format(),
  ];

  useEffect(() => {
    fetchShowTime();
  }, []);

  const fetchShowTime = () => {
    setLoading(true);
    axios
      .get(
        'http://movietome.unomy.pw/showtime/movie/' +
        _id +
        '?date=' +
        currentDate,
      )
      .then(response => {
        console.log(response);
        setData(response.data);
        const List = [...new Set(response.data.map(value => value.cinemaId))];
        setCinemaList(List);
      })

      .catch(error => {
        setLoading(false);
        console.log(error);
      })
      .then(() => {
        setLoading(false);
      });
  };

  const RenderShowTime = () => {
    return days.map((value, index) => {
      if (index === 0) {
        return (
          <ShowTime
            heading="Today"
            day={value}
            movieId={_id}
            key={index}
            tabStyle={{ backgroundColor: '#202020' }}
            textStyle={{ color: '#DADADA' }}
            activeTabStyle={{ backgroundColor: '#202020' }}
            activeTextStyle={{ color: '#9F3535' }} />
        );
      } else if (index === 1) {
        return (
          <ShowTime
            heading="Tomorrow"
            day={value}
            movieId={_id}
            key={index}
            tabStyle={{ backgroundColor: '#202020' }}
            textStyle={{ color: '#DADADA' }}
            activeTabStyle={{ backgroundColor: '#202020' }}
            activeTextStyle={{ color: '#9F3535' }} />
        );
      } else {
        return (
          <ShowTime
            heading={moment(value).format('ddd D')}
            day={value}
            movieId={_id}
            key={index}
            tabStyle={{ backgroundColor: '#202020' }}
            textStyle={{ color: '#DADADA' }}
            activeTabStyle={{ backgroundColor: '#202020' }}
            activeTextStyle={{ color: '#9F3535' }} />
        );
      }
    });
  };
  return (
    <>
      <Appbar
        style={{
          height: 40,
          alignItems: 'center',
          paddingTop: 50,
          paddingBottom: 30,
          justifyContent: 'center',
        }}>
        <Appbar.Content
          style={{ alignItems: 'center' }}
          title="Select Showtime"
        />
      </Appbar>

      <ImageBackground
        source={{ uri: image }}
        style={{
          height: 130,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: 'rgba( 0, 0, 0, 0.7 )',
            padding: 12,
          }}>
          <View style={{ flex: 1 }}>
            <Image
              source={{ uri: image }}
              style={{ ...StyleSheet.absoluteFillObject }}
            />
          </View>
          <View
            style={{
              flex: 3,
              padding: 10,
            }}>
            <Text style={{ fontWeight: '700' }}>{name}</Text>
            <Text>{duration} mins</Text>
            <Text>Rating : {rating}</Text>
          </View>
        </View>
      </ImageBackground>
      {data ? (
        <Tabs
          tabBarUnderlineStyle={{ backgroundColor: '#9F3535' }}
          renderTabBar={() => <ScrollableTab />}>
          {RenderShowTime()}
        </Tabs>
      ) : (
          <ActivityIndicator
            animating={loading}
            style={styles.loader}
            size="large"
          />
        )}
    </>
  );
};

export default MovieDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, // take up all screen
    paddingTop: 20, // start below status bar
  },
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
  },
  scrollContent: {
    flexDirection: 'row', // arrange posters in rows
    flexWrap: 'wrap', // allow multiple rows
  },
});
