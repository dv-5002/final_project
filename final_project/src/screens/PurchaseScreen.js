import React from 'react';
import {View, Text, Image, BackHandler, Alert} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import {useRoute, useNavigation, StackActions} from '@react-navigation/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import moment from 'moment';
import axios from 'axios';
import {Button} from 'react-native-paper';
import {
  faVolumeUp,
  faClosedCaptioning,
  faClock,
  faVideo,
  faMapMarkerAlt,
} from '@fortawesome/free-solid-svg-icons';

const PurchaseScreen = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const {purchaseData} = route.params;
  console.log(purchaseData._id);

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          height: 40,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontWeight: 'bold'}}>Purchase Successfully</Text>
      </View>

      <View
        style={{
          height: 130,
          flexDirection: 'row',
          padding: 10,
          backgroundColor: '#fbfcfe',
        }}>
        <View
          style={{
            flex: 1,
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 10,
            backgroundColor: 'white',
          }}>
          <Image
            source={{uri: purchaseData.showtime.movie.image}}
            style={{flex: 1}}
          />
        </View>

        <View style={{flex: 3, paddingHorizontal: 10}}>
          <Text style={{fontSize: 12, fontWeight: 'bold'}}>
            {moment(purchaseData.startDateTime).format('DD MMMM YYYY | HH:mm')}{' '}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faVideo} size={12} />
            </View>
            <Text style={{fontSize: 12}}>
              {purchaseData.showtime.theater.name}
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faMapMarkerAlt} size={12} />
            </View>
            <Text style={{fontSize: 12}}>
              {purchaseData.showtime.cinema.name}
            </Text>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faClosedCaptioning} size={12} />
            </View>

            <Text
              style={{
                fontSize: 12,
              }}>
              {purchaseData.showtime.subtitle} |
            </Text>
            <View style={{marginHorizontal: 5}}>
              <FontAwesomeIcon icon={faVolumeUp} size={12} />
            </View>

            <Text style={{fontSize: 12}}>
              {purchaseData.showtime.soundtrack}
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 5}}>
              <FontAwesomeIcon icon={faClock} size={12} />
            </View>
            <Text style={{fontSize: 12}}>
              {purchaseData.showtime.movie.duration} mins
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          height: 40,
          backgroundColor: '#eef3f6',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{alignItems: 'center', marginRight: 10}}>
          <Image
            style={{
              width: 20,
              height: 20,
            }}
            source={require('src/assets/lounge_blue.png')}
          />
          <Text style={{fontSize: 7}}>Normal</Text>
        </View>
        <View style={{alignItems: 'center', marginRight: 10}}>
          <Image
            style={{
              width: 20,
              height: 20,
            }}
            source={require('src/assets/lounge_red.png')}
          />
          <Text style={{fontSize: 7}}>Deluxe</Text>
        </View>
        <View style={{alignItems: 'center', marginRight: 10}}>
          <Image
            style={{
              width: 20,
              height: 20,
            }}
            source={require('src/assets/lounge_green.png')}
          />
          <Text style={{fontSize: 7}}>Sofa</Text>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fbfcfe',
          padding: 10,
        }}>
        <Text style={{fontWeight: 'bold'}}>Seats</Text>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {purchaseData
            ? purchaseData.seats.map((value, index) => (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginRight: 2,
                  }}
                  key={index}>
                  {value.type === 'NORMAL' ? (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                      }}
                      source={require('src/assets/lounge_blue.png')}
                    />
                  ) : (
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                      }}
                      source={
                        value.type === 'DELUXE'
                          ? require('src/assets/lounge_red.png')
                          : require('src/assets/lounge_green.png')
                      }
                    />
                  )}

                  <Text>{`(${value.row},${value.column})`}</Text>
                </View>
              ))
            : null}
        </View>
        <View
          style={{
            marginVertical: 30,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <QRCode value={purchaseData._id} size={150} />
          <Text style={{marginVertical: 20}}>
            Booking id : {purchaseData._id}
          </Text>
          <Button
            mode="outlined"
            onPress={() =>
              navigation.dispatch(StackActions.replace('MainRouter'))
            }>
            close
          </Button>
        </View>
      </View>
    </View>
  );
};

export default PurchaseScreen;
