import React, { useEffect, useState } from 'react';
import { RefreshControl, ScrollView, StyleSheet, View, TouchableOpacity, ImageBackground } from 'react-native';
import { ActivityIndicator, Text } from 'react-native-paper'
import Header from 'src/components/Header';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import { FlatGrid } from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient';

const MoviesScreen = () => {
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  useEffect(() => {
    fetchMovies();
  }, []);
  const fetchMovies = () => {
    setLoading(true);
    axios.get('http://movietome.unomy.pw/movie')
      .then(response => {
        console.log(response);
        setMovies(response.data);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      })
      .then(() => {
        setLoading(false);
      });
  };
  return (
    <View style={styles.container}>
      {movies ? (
        <ScrollView
          contentContainerStyle={styles.scrollContent}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={fetchMovies} />
          }>
          <FlatGrid
            itemDimension={120}
            items={movies}
            style={styles.gridView}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('MovieDetail', {
                    item: item
                  });
                }}
              >
                <ImageBackground source={{ uri: item.image }} style={[styles.itemContainer]}>
                  <LinearGradient style={{ height: 100, justifyContent: 'flex-end', }} colors={['#000', 'transparent']} start={{ x: 0.5, y: 0.7 }} end={{ x: 0.5, y: 0 }}>
                    <Text style={styles.itemName}>{item.name}</Text>
                    <Text style={styles.itemCode}>{item.code}</Text>
                  </LinearGradient>
                </ImageBackground >
              </TouchableOpacity>

            )}
          />
        </ScrollView>
      ) : (
          <ActivityIndicator
            animating={loading}
            style={styles.loader}
            size="large"
          />
        )}
    </View>
  );
};

export default MoviesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1a1b1f'
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  scrollContent: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10
  },
  gridView: {
    flex: 1,

  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    height: 240,
  },
  itemName: {
    fontSize: 15,
    paddingHorizontal: 12
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
  },
});
