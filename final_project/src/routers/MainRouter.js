import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MoviesScreen from './node_modules/src/screens/MoviesScreen';
import CinemaScreen from './node_modules/src/screens/CinemaScreen';
import MapScreen from './node_modules/src/screens/MapScreen';
import Header from '../components/Header';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faMapMarkerAlt,
  faPlayCircle,
  faBars,
  faLocationArrow,
} from '@fortawesome/free-solid-svg-icons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MoreScreen from '../screens/MoreScreen';

const Tab = createMaterialBottomTabNavigator();
const MainRouter = () => {
  return (
    <>
      <Header />
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#D22C2C"
        shifting={false}
        inactiveColor="#ccc"
        unmountOnBlur={true}
        barStyle={{
          backgroundColor: '#202020',
          fontWeight: '700',
        }}>
        <Tab.Screen
          name="Movies"
          component={MoviesScreen}
          options={{
            tabBarLabel: 'Now Showing',
            labelStyle: {fontSize: 10},
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faPlayCircle} color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name="Cinema"
          component={CinemaScreen}
          options={{
            tabBarLabel: 'Cinema',
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faMapMarkerAlt} color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name="Map"
          component={MapScreen}
          options={{
            tabBarLabel: 'Map',
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faLocationArrow} color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name="More"
          component={MoreScreen}
          options={{
            tabBarLabel: 'More',
            tabBarIcon: ({color, size}) => (
              <FontAwesomeIcon icon={faBars} color={color} size={20} />
            ),
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default MainRouter;
