import React from 'react';
import { View, Text, Image, StatusBar } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import MainRouter from './MainRouter';
import MovieDetailScreen from 'src/screens/MovieDetailScreen';
import BookingScreen from 'src/screens/BookingScreen';
import CinemaDetailScreen from 'src/screens/CinemaDetailScreen';
import PurchaseScreen from 'src/screens/PurchaseScreen';
import HistoryScreen from 'src/screens/HistoryScreen';
import HistoryDetailScreen from 'src/screens/HistoryDetailScreen';
const Stack = createStackNavigator();
const StackRouter = () => {
  return (
    <>
      <StatusBar translucent backgroundColor="transparent" />
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="MainRouter" component={MainRouter} />
        <Stack.Screen name="MovieDetail" component={MovieDetailScreen} />
        <Stack.Screen name="Booking" component={BookingScreen} />
        <Stack.Screen name="CinemaDetail" component={CinemaDetailScreen} />
        <Stack.Screen name="Purchase" component={PurchaseScreen} />
        <Stack.Screen name="History" component={HistoryScreen} />
        <Stack.Screen name="HistoryDetail" component={HistoryDetailScreen} />
      </Stack.Navigator>
    </>
  );
};

export default StackRouter;
