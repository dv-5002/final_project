import AsyncStorage from '@react-native-community/async-storage';
import {
  checkAsyncStorageSuccess,
  checkAsyncStorageBegin,
  checkAsyncStorageError,
  fetchUserActionBegin,
  fetchUserActionError,
  fetchUserActionSuccess,
  updateBegin,
  updateError,
  updateSuccess,
  loginSuccess,
  loginBegin,
  loginError,
  LOGIN_BEGIN,
} from '../actions/authAction';
import {
  CHECK_LOGIN_BEGIN,
  CHECK_LOGIN_SUCCESS,
  CHECK_LOGIN_ERROR,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  FETCH_USER_BEGIN,
  FETCH_USER_ERROR,
  FETCH_USER_SUCCESS,
  UPDATE_BEGIN,
  UPDATE_ERROR,
  UPDATE_SUCCESS,
  LOGOUT,
} from '../actions/authAction';

import axios from 'axios';

export const storeData = async data => {
  try {
    await AsyncStorage.setItem('token', data);
  } catch (e) {
    alert(e);
  }
};

export const login = (username, password) => {
  return dispatch => {
    dispatch(loginBegin());
    axios
      .post('http://movietome.unomy.pw/auth/login', {username, password})
      .then(response => {
        console.log('retrived Token', response.data.token);
        dispatch(loginSuccess(response.data.token));
      })

      .catch(error => {
        if (error.message === 'Network Error') {
          alert('Network error , please check your internet connection!');
        } else if (
          error.response.data.messages.includes('incorrect/username')
        ) {
          alert('incorrect username');
          console.log(JSON.stringify(error));
        } else if (
          error.response.data.messages.includes('incorrect/password')
        ) {
          alert('incorrect password');
        } else {
          alert(error.response.data.messages);
        }
        console.log(JSON.stringify(error));
        dispatch(loginError(error));
      });
  };
};

export const fetchUser = token => {
  return dispatch => {
    dispatch(fetchUserActionBegin());
    axios
      .get('http://movietome.unomy.pw/user/profile', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(response => {
        console.log('user', response.data);
        dispatch(fetchUserActionSuccess(response.data));
      })
      .catch(error => {
        console.log('fetch Error1234', JSON.stringify(error));
        dispatch(fetchUserActionError(error));
      });
  };
};

export const logoutActionCreator = async token => {
  //const token = await AsyncStorage.getItem('token');
  axios
    .post('http://koolpiggybank.unomy.pw/auth/logout', {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    });

  return AsyncStorage.removeItem('token', err => console.log('error', err));
};

const INITIAL_STATE = {
  user: {},
  token: null,
  isLogin: false,
  err: undefined,
  fetchingUser: false,
};

export const AuthReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_BEGIN:
      return {
        ...state,
      };
    case LOGIN_SUCCESS:
      // storeData(action.payLoad);

      return {
        ...state,
        token: action.payLoad,
        isLogin: true,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        err: action.payLoad,
      };

    case LOGOUT:
      logoutActionCreator(action.payLoad);
      return INITIAL_STATE;
    case FETCH_USER_BEGIN:
      return {
        ...state,
        fetchingUser: true,
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        user: action.payLoad,
        fetchingUser: false,
      };
    case FETCH_USER_ERROR:
      return {
        ...state,
        isLogin: false,
        fetchingUser: false,
      };
    default:
      return state;
  }
};
