import React from 'react';
import {View, Text, Image} from 'react-native';
import { Appbar } from 'react-native-paper';

const HeaderLogo = () => {
  return (
    <Appbar 
      style={{
        height: 40,
        alignItems: 'center',
        paddingTop: 50,
        paddingBottom:30,
        justifyContent: 'center',
      }}>
      <Image
        source={require('src/assets/icon.png')}
        style={{width: 30, height: 30}}
      />
    </Appbar >
  );
};

export default HeaderLogo;
