import React, { useState, useEffect } from 'react';
import { View, Image } from 'react-native';
import { Avatar } from 'react-native-paper'
import { TouchableOpacity } from 'react-native-gesture-handler';

const Seat = ({ type, addSeat, deleteSeat, status, row, col }) => {
  switch (type) {
    case 'NORMAL':
      return status === 'book' ? (
        <Image
          source={{ uri: 'https://cdn1.iconfinder.com/data/icons/instagram-ui-colored/48/JD-17-512.png' }}
          style={{ width: 15, height: 15 }} />
      ) : (
          <TouchableOpacity
            onPress={() => {
              status === 'selected'
                ? deleteSeat('NORMAL', row, col)
                : addSeat('NORMAL', row, col);
            }}>
            <Image
              source={{ uri: status=== 'selected' ? 'https://img.icons8.com/flat_round/600/000000/checkmark.png' : 'https://img.icons8.com/offices/600/000000/theatre-seat.png'}}
              style={{ width: 15, height: 15 }} />
          </TouchableOpacity>
        );

    case 'DELUXE':
      return status === 'book' ? (
        <Image
          source={{ uri: 'https://cdn1.iconfinder.com/data/icons/instagram-ui-colored/48/JD-17-512.png' }}
          style={{ width: 15, height: 15 }} />
      ) : (
          <TouchableOpacity
            onPress={() => {
              status === 'selected'
                ? deleteSeat('DELUXE', row, col)
                : addSeat('DELUXE', row, col);
            }}>
              <Image
              source={{ uri: status=== 'selected' ? 'https://img.icons8.com/flat_round/600/000000/checkmark.png' : 'https://img.icons8.com/dusk/600/000000/armchair.png'}}
              style={{ width: 15, height: 15 }} />
            </TouchableOpacity>
        );

    case 'SOFA':
      return status === 'book' ? (
        <Image
          source={{ uri: 'https://cdn1.iconfinder.com/data/icons/instagram-ui-colored/48/JD-17-512.png' }}
          style={{ width: 20, height: 20 }} />
      ) : (
          <TouchableOpacity
            onPress={() => {
              console.log('SOFA PRESS');
              if (status === 'selected') {
                deleteSeat('SOFA', row, col);
              } else {
                addSeat('SOFA', row, col);
              }
            }}>
              <Image
              source={{ uri: status=== 'selected' ? 'https://img.icons8.com/flat_round/600/000000/checkmark.png' : 'https://img.icons8.com/dusk/600/000000/sofa.png'}}
              style={{ width: 20, height: 20 }} />
            </TouchableOpacity>
        );

    default:
      return null;
  }
};

export default Seat;
