import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  Image,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import {Tab} from 'native-base';
import axios from 'axios';

const CinemaShowTIme = ({cinemaId, day}) => {
  const navigation = useNavigation();
  const [showTime, setShowtime] = useState();
  const [loadingShowTime, setLoadingShowTime] = useState(false);
  const [movieList, setMovieList] = useState();
  useEffect(() => {
    fetchShowTime();
    console.log('day', day);
  }, []);
  const fetchShowTime = () => {
    let date = new Date(day).getTime();
    setLoadingShowTime(true);
    axios
      .get(
        'http://movietome.unomy.pw/showtime/cinema/' +
          cinemaId +
          '?date=' +
          date,
      )
      .then(response => {
        setShowtime(response.data);
        console.log('response', response.data);

        const List = [...new Set(response.data.map(value => value.movieId))];

        setMovieList(List);
      })
      .catch(error => {
        console.log(error);
        setLoadingShowTime(false);
      })
      .finally(() => {
        setLoadingShowTime(false);
      });
  };

  const FilterArray = ({movieId}) => {
    let movieData = showTime.find(value => value.movieId === movieId);

    const {movie} = movieData;

    let target = {};
    showTime.forEach(showtime => {
      const targetMovie = target[showtime.movieId];
      if (targetMovie) {
        const targetTheater = targetMovie[showtime.theaterId];
        if (targetTheater) {
          targetTheater.showtimes.push({
            id: showTime._id,
            startDateTime: showTime.startDateTime,
          });
        } else {
          targetMovie.theaters[showtime.theaterId] = {
            name: showtime.theater.name,
            id: showtime.theater._id,
            showtimes: [
              {
                startDateTime: showtime.startDateTime,
                id: showtime._id,
              },
            ],
          };
        }
      } else {
        target[showtime.movieId] = {
          name: showtime.movie.name,
          id: showtime.movieId,
          theaters: {
            [showtime.theaterId]: {
              name: showtime.theater.name,
              id: showtime.theater._id,
              showtimes: [
                {
                  startDateTime: showtime.startDateTime,
                  id: showtime._id,
                },
              ],
            },
          },
        };
      }
    });
    console.log('target', target);
    return Object.values(target).map((item, index) => {
      //console.log(item.name + ' theater ', Object.values(item.theaters));
      if (item.id === movieId) {
        return (
          <View>
            <ImageBackground
              source={{uri: movie.image}}
              style={{
                height: 130,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  backgroundColor: 'rgba( 0, 0, 0, 0.6 )',
                  padding: 12,
                }}>
                <View style={{flex: 1}}>
                  <Image
                    source={{uri: movie.image}}
                    style={{...StyleSheet.absoluteFillObject}}
                  />
                </View>
                <View
                  style={{
                    flex: 3,
                    padding: 10,
                  }}>
                  <Text style={{fontWeight: '700', color: 'white'}}>
                    {movie.name}
                  </Text>
                  <Text style={{color: 'white'}}>{movie.duration} mins</Text>
                  <Text style={{color: 'white'}}>Rating : {movie.rating}</Text>
                </View>
              </View>
            </ImageBackground>
            <View
              style={{
                backgroundColor: 'white',
                paddingHorizontal: 10,
                paddingVertical: 5,
              }}
              key={index}>
              {Object.values(item.theaters).map((theater, theaterindex) => {
                console.log(theater);
                let temp = theater.showtimes.filter(
                  (upComing, index) =>
                    new Date(upComing.startDateTime) > new Date(),
                );
                console.log('temp', temp);
                return (
                  <View key={theaterindex}>
                    <Text style={{fontWeight: 'bold'}}>{theater.name}</Text>
                    {theater.showtimes.map((showtime, index) => {
                      const startTime = new Date(showtime.startDateTime);
                      return startTime > new Date() ? (
                        <TouchableOpacity
                          style={{
                            width: 80,
                            height: 40,
                            margin: 5,
                            padding: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 1,
                            elevation: 4,
                          }}
                          onPress={() =>
                            navigation.navigate('Booking', {
                              showtime: showtime.id,
                            })
                          }
                          key={index}>
                          <LinearGradient
                            colors={['#ff5e00', '#c8cc28']}
                            start={{x: 0, y: 0}}
                            end={{x: 1, y: 0}}
                            style={{
                              ...StyleSheet.absoluteFillObject,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text style={{color: 'white', fontWeight: '700'}}>
                              {startTime.getHours() +
                                ':' +
                                (startTime.getMinutes() < 10 ? '0' : '') +
                                startTime.getMinutes()}
                            </Text>
                          </LinearGradient>
                        </TouchableOpacity>
                      ) : (
                        <View
                          style={{
                            width: 80,
                            height: 40,
                            margin: 5,
                            padding: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 1,
                            backgroundColor: '#ccc',
                            elevation: 4,
                          }}
                          onPress={() =>
                            navigation.navigate('Booking', {
                              showtime: showtime.id,
                            })
                          }
                          key={index}>
                          <Text style={{color: 'white', fontWeight: '700'}}>
                            {startTime.getHours() +
                              ':' +
                              (startTime.getMinutes() < 10 ? '0' : '') +
                              startTime.getMinutes()}
                          </Text>
                        </View>
                      );
                    })}
                  </View>
                );
              })}
            </View>
          </View>
        );
      }
    });
  };

  return (
    <Tab>
      {loadingShowTime ? (
        <ActivityIndicator
          animating={loadingShowTime}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
          }}
          size="large"
        />
      ) : (
        <FlatList
          data={movieList ? movieList : []}
          renderItem={({item}) => <FilterArray movieId={item} />}
          keyExtractor={(item, index) => index + ''}
        />
      )}
    </Tab>
  );
};

export default CinemaShowTIme;
