import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, } from 'react-native';
import { Text, ActivityIndicator, Button } from 'react-native-paper';
import { Tab } from 'native-base';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ShowTime = props => {
  const navigation = useNavigation();
  const [cinemaList, setCinemaList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const { day, movieId } = props;

  useEffect(() => {
    fetchData();
  }, []);
  //console.log(movieId);
  console.log(new Date(day).getTime());

  const fetchData = () => {
    setLoading(true);
    axios
      .get(
        'http://movietome.unomy.pw/showtime/movie/' +
        movieId +
        '?date=' +
        new Date(day).getTime(),
      )
      .then(response => {
        console.log(response);
        setData(response.data);
        const List = [...new Set(response.data.map(value => value.cinemaId))];
        //console.log('cinema List ', List);
        setCinemaList(List);
      })

      .catch(error => {
        setLoading(false);
        console.log(error);
      })
      .then(() => {
        setLoading(false);
      });
  };
  const RenderCard = ({ cinemaId }) => {
    const showtime = data.filter(value => value.cinemaId === cinemaId);
    const upcoming = showtime.filter((value, index) => {
      return new Date(value.startDateTime) > new Date();
    });
    const sortDate = upcoming.sort(
      (a, b) => new Date(a.startDateTime) - new Date(b.startDateTime),
    );
    const theaterList = [...new Set(sortDate.map(value => value.theater.id))];

    if (sortDate.length > 0) {
      return (
        <View
          style={{
            height: null,
            flexDirection: 'row',
            backgroundColor: '#1a1b1f',
          }}>
          <View style={{ flex: 1 }}>
            <View
              style={{
                backgroundColor: '#626262',
                height: 40,
                justifyContent: 'center',
                padding: 5,
              }}>
              <Text
                style={{ fontWeight: '700', marginLeft: 15 }}>
                {showtime[0].cinema.name}
              </Text>
            </View>
            {theaterList.map((theaterId, index) => {
              const showtimeButton = sortDate.filter(
                value => value.theaterId === theaterId,
              );

              return (
                <View
                  key={index}
                  style={{
                    // alignItems: 'center',
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                  }}>
                  <Text style={{ fontWeight: '700', color: '#494949' }}>
                    {showtimeButton[0].theater.name}
                  </Text>
                  <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {showtimeButton.map((value, index) => {
                      const time = new Date(value.startDateTime);
                      if (index === 0) {
                        return (
                          <Button
                            mode="contained"
                            style={{
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                            onPress={() =>
                              navigation.navigate('Booking', {
                                showtime: value._id,
                              })
                            }
                            key={index}>
                            <Text style={{fontWeight: '700' }}>
                              {time.getHours() +
                                ':' +
                                (time.getMinutes() < 10 ? '0' : '') +
                                time.getMinutes()}
                            </Text>
                          </Button>
                        );
                      } else {
                        return (
                          <Button
                            mode="outlined"
                            style={{
                              marginLeft: 5,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                            onPress={() =>
                              navigation.navigate('Booking', {
                                showtime: value._id,
                              })
                            }
                            key={index}>
                            <Text>
                              {time.getHours() +
                                ':' +
                                (time.getMinutes() < 10 ? '0' : '') +
                                time.getMinutes()}
                            </Text>
                          </Button>
                        );
                      }
                    })}
                  </View>
                </View>
              );
            })}
          </View>
        </View>
      );
    } else {
      return null;
    }
  };
  return loading ? (
    <ActivityIndicator animating={loading} style={styles.loader} size="large" />
  ) : (
      <Tab>
        {cinemaList.length == 0 ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
              backgroundColor:'#202020'
            }}>
            <Text>No Movie Showtime</Text>
          </View>
        ) : (
            <FlatList
              data={cinemaList ? cinemaList : []}
              renderItem={({ item }) => <RenderCard cinemaId={item} />}
              keyExtractor={(item, index) => index + ''}
              style={{backgroundColor:'#202020'}}
            />
          )}
      </Tab>
    );
};

export default ShowTime;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    alignItems: 'center', // center horizontally
    justifyContent: 'center', // center vertically
    backgroundColor:'#202020'
  },
});
